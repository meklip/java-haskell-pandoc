# HSConvert
Small library to convert data using the pandoc api

## Build
Use ghcup to install ghc and cabal > v2.

Build the project with
```
cabal v2-build
```

## Resources
+ [Cabal foreign libraries](https://cabal.readthedocs.io/en/latest/developing-packages.html#foreign-libraries)
+ [Blog post about using haskell libraries in c](https://ro-che.info/articles/2017-07-26-haskell-library-in-c-project)
+ [Haskell foreign libraries tutorial](https://github.com/ifesdjeen/haskell-ffi-tutorial)

## Pitfalls
+ Using stack: stack semms to have a lot of problems with linking libraries, better to use cabal-install.
+ `foreign-library` cabal directive is not available in the bundled version from apt on ubuntu, use ghcup instead.
+ Use v2-* cabal commands to build the project, defaults can cause conflicts.
+ Cabal can't build static libraries at the moment.
+ Haskell runtime has to be initialized before the library functions for conversion can be used. Call `hsConvertInit()` and `hsConvertExit()`.
