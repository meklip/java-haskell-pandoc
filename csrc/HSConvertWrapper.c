#include <stdlib.h>
#include "HsFFI.h"

// TODO windows dllmain

void hsConvertInit() {
  int argc = 2;

  char *aargv[] = { "+RTS", "-A32m", NULL };
  char **argv = aargv;

  // Initialize Haskell runtime
  hs_init(&argc, &argv);
}

void hsConvertExit() {
  hs_exit();
}
