#ifndef HSCONVERT
#define HSCONVERT 1

#ifdef __cplusplus
extern "C" {
#endif

void hsConvertInit();
void hsConvertExit();

char* h2n(char* str);
char* n2md(char* str);

#ifdef __cplusplus
}
#endif

#endif