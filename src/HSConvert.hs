module HSConvert where

import Foreign.C.String


import Text.Pandoc
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

foreign export ccall h2n :: CString -> IO CString
foreign export ccall n2md :: CString -> IO CString

h2n :: CString -> IO CString
h2n str = do
    input <- peekCString str
    result <- runIO $ do
        doc <- readHtml def (T.pack input)
        writeNative def doc
    resultStr <- handleError result
    newCString (T.unpack resultStr)

n2md :: CString -> IO CString
n2md str = do
    input <- peekCString str
    result <- runIO $ do
        doc <- readNative def (T.pack input)
        writeMarkdown def doc
    resultStr <- handleError result
    newCString (T.unpack resultStr)