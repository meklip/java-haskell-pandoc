import com.sun.jna.Library;
import com.sun.jna.Native;

public class HSBridge {

    public interface HSConvert extends Library {
        HSConvert INSTANCE = Native.load("hsconvert", HSConvert.class);

        void hsConvertInit();
        void hsConvertExit();

        String h2n(String input);
        String n2md(String input);
    }

    public static void main(String[] args) {
        long totalTimeStart = System.currentTimeMillis();
        HSConvert.INSTANCE.hsConvertInit();

        long startTime = System.currentTimeMillis();

        String nt = "";
        for (int i = 0; i < 100; i++) {
            nt = HSConvert.INSTANCE.h2n("<p><div><h2>Hallo Ü</h2></div></p>");
        }
        long endTime = System.currentTimeMillis();

        System.out.println("Output: " + nt);
        System.out.println("Avg. execution time: " + Double.toString((endTime - startTime) / 100.0) + "ms");

        String md = HSConvert.INSTANCE.n2md(nt);
        System.out.println("Markdown: " + md);

        HSConvert.INSTANCE.hsConvertExit();
        long totalTimeEnd = System.currentTimeMillis();

        System.out.println("Total time: " + Long.toString(totalTimeEnd - totalTimeStart) + "ms");
    }

}
